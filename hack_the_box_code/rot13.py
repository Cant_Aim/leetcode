import base64

class Solution(object):
    original = "abcdefghijklmnopqrstuvwxyz"
    processed = "nopqrstuvwxyzabcdefghijklm"

    def operate(self, message):
        changed = ""
        for c in message.lower():
            i = self.original.find(c)
            print(i)
            if i > -1:
                changed = changed + self.processed[i]
            else:
                changed = changed + c
        print(changed)

Solution().operate("Va beqre gb trarengr gur vaivgr pbqr, znxr n CBFG erdhrfg gb /ncv/vaivgr/trarengr")

print(base64.b64decode("SEhYU1ctS0JISlotWEZBT1QtV0tLWVAtUVdGV0g="))
