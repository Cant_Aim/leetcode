class Solution(object):
    def num_of_possible_decoding(self, input):
        """
        :type input: string
        :rtype: int
        """
        m = [None for i in range(len(input))]
        return self.count(input, 0, m)

    def count(self, input, k, m):
        if not len(input):
            return 1
        if m[k] is not None:
            return m[k]
        result = self.count(input[1:], k + 1, m)
        if len(input) > 1 and int(input[0:2]) < 27:
            result += self.count(input[2:], k + 2, m)
        m[k] = result
        return result


print(Solution().num_of_possible_decoding("1223"))
